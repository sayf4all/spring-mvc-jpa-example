<#import "/spring.ftl" as spring>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"[]>
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="en-US" xml:lang="en">
<head>
    <!--
    Created by Artisteer v3.0.0.45570
    Base template (without user's data) checked by http://validator.w3.org : "This page is valid XHTML 1.0 Transitional"
    -->
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title>${pagetitel!"page title empty!"}</title>
    <meta name="description" content="Description"/>
    <meta name="keywords" content="Keywords"/>


    <link rel="stylesheet" href="/static/style.css" type="text/css" media="screen"/>
    <!--[if IE 6]>
  <link rel="stylesheet" href="/static/style.ie6.css" type="text/css" media="screen"/><![endif]-->
    <!--[if IE 7]>
  <link rel="stylesheet" href="/static/style.ie7.css" type="text/css" media="screen"/><![endif]-->
    <link media="screen" type="text/css" href="/static/style.css" rel="stylesheet">
    <script src="/static/jquery.js" type="text/javascript"></script>
    <script src="/static/script.js" type="text/javascript"></script>
    <script src="/static/js/all.js"></script>


</head>
<body>
<div id="mvc-main">


    <h1 class="mvc-logo-name"><a href="#"></a></h1>

    <h2 class="mvc-logo-text"></h2>

    <div class="cleared reset-box"></div>
    <div class="mvc-header">
        <div class="mvc-header-clip">
            <div class="mvc-header-center">
                <div class="mvc-header-png"></div>
                <div class="mvc-header-jpeg"></div>
            </div>
        </div>
        <div class="mvc-header-wrapper">
            <div class="mvc-header-inner">
                <a href="/">
                    <div class="mvc-headerobject"></div>
                </a>

                <div class="mvc-logo">
                    <h1 id="header-tekst" class="mvc-logo-name"><a href="/"><@spring.message 'common.siteheader'/></a>
                    </h1>

                    <h2 id="slogan-tekst" class="mvc-logo-text"><@spring.message 'common.sitesubheader'/></h2>
                </div>
            </div>
        </div>
    </div>

    <div class="cleared reset-box"></div>
    <div class="mvc-sheet">
        <div class="mvc-sheet-tl"></div>
        <div class="mvc-sheet-tr"></div>
        <div class="mvc-sheet-bl"></div>
        <div class="mvc-sheet-br"></div>
        <div class="mvc-sheet-tc"></div>
        <div class="mvc-sheet-bc"></div>
        <div class="mvc-sheet-cl"></div>
        <div class="mvc-sheet-cr"></div>
        <div class="mvc-sheet-cc"></div>
        <div class="mvc-sheet-body">


        <#include "navigation.ftl">

            <div class="cleared reset-box"></div>
            <div class="mvc-content-layout">
                <div class="mvc-content-layout-row">


                <#include "sidebar-left.ftl">


                    <div class="mvc-layout-cell mvc-content">
                        <div class="mvc-post">
                            <div class="mvc-post-body">
                                <div class="mvc-post-inner mvc-article">
                                    <h2 class="mvc-postheader">
                                    ${pagetitel!"page title empty!"}
                                    </h2>

                                    <div class="cleared"></div>
                                    <div class="mvc-postcontent">
<#include "messages.ftl">