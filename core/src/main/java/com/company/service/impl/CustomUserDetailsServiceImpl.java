package com.company.service.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.company.data.repository.UserRepository;
import com.company.service.CustomUserDetailsService;


@Service
@Transactional(readOnly = true)
public class CustomUserDetailsServiceImpl implements CustomUserDetailsService {

  private static final Logger LOG = LoggerFactory.getLogger(CustomUserDetailsServiceImpl.class);

  @Autowired
  private UserRepository userRepository;


  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
    try {
      com.company.data.model.users.User user = userRepository.findByUsername(username);

      return getUserDetailsObject(user);
    }
    catch (Exception e) {
      throw new RuntimeException(e);
    }
  }


  public User getUserDetailsObject(com.company.data.model.users.User user) {
    User springSecurityUserDetails;
    boolean enabled = true;
    boolean accountNonExpired = true;
    boolean credentialsNonExpired = true;
    boolean accountNonLocked = true;
    String name = user.getUsername();
    String password = user.getPassword().toLowerCase();
    List<? extends GrantedAuthority> authorities = user.getAuthorities();

    springSecurityUserDetails = new User(name, password, enabled, accountNonExpired, credentialsNonExpired, accountNonLocked,
        authorities);
    return springSecurityUserDetails;
  }


  public Collection<? extends GrantedAuthority> grantAuthorities(Long role) {
    List<GrantedAuthority> authList = getGrantedAuthorities(getRoles(role));
    return authList;
  }


  public List<String> getRoles(Long role) {
    List<String> roles = new ArrayList<String>();

    if (role.intValue() == 1) {
      roles.add("ROLE_USER");
      roles.add("ROLE_ADMIN");

    }
    else if (role.intValue() == 2) {
      roles.add("ROLE_USER");
    }

    return roles;
  }


  public List<GrantedAuthority> getGrantedAuthorities(List<String> roles) {
    List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
    for (String role : roles) {
      authorities.add(new SimpleGrantedAuthority(role));
    }
    return authorities;
  }

}
