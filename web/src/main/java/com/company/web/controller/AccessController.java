package com.company.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import com.company.web.controller.util.Paths;


@Controller
@RequestMapping
public class AccessController extends AbstractController {
  //templates
  public static final String ERROR_PAGES_403_TEMPLATE = "error-pages/403";
  public static final String ACCESS_LOGIN_TEMPLATE = "loginform";
  public static final String REDIRECT_LOGIN_WITH_MESSAGE_TEMPLATE = "redirect:/login?message=";
  private static final Logger LOG = LoggerFactory.getLogger(AccessController.class);


  @RequestMapping(value = Paths.LOGIN, method = RequestMethod.GET)
  public String login(Model model, @RequestParam(required = false) String message) {
    model.addAttribute("message", message);
    UserDetails userDetails = getCurrentUserDetails();
    if (userDetails != null) {
      LOG.info("Succesfully logged in user: [" + userDetails.getUsername() + "]");
    }
    return ACCESS_LOGIN_TEMPLATE;
  }


  @RequestMapping(value = Paths.ACCESS_DENIED)
  public String denied(Model model) {
    setPageAttributes(model, "access denied");
    return ERROR_PAGES_403_TEMPLATE;
  }


  @RequestMapping(value = Paths.LOGIN_FAILURE)
  public String loginFailure() {
    String message = "Login Failure!";
    return REDIRECT_LOGIN_WITH_MESSAGE_TEMPLATE + message;
  }


  @RequestMapping(value = Paths.LOGOUT_SUCCESS)
  public String logoutSuccess() {
    String message = "Logout Success!";
    return REDIRECT_LOGIN_WITH_MESSAGE_TEMPLATE + message;
  }

}
