package com.company.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import com.company.web.controller.util.Paths;


@Controller
public class ContactController extends AbstractController {
  public static final String CONTACT_CONTACT_TEMPLATE = "contact";
  private String pageTitle = "Contact";


  @RequestMapping(value = Paths.CONTACT)
  public String showContactPage(Model model) {
    setPageAttributes(model, pageTitle);
    return CONTACT_CONTACT_TEMPLATE;
  }

}
