<#include 'common/header.ftl'>
<#if beansMap??>
<table>
    <caption><@spring.message 'item.overview.page.title'/></caption>
    <thead>
    <tr>
      <#list beansMap["beanFields"] as field>
        <#if field["name"]=="authorities">
        <#elseif field["name"]=="encoder">
        <#else>
            <td>${field["name"]}</td>
        </#if>
      </#list>
        <td>actie</td>
    </tr>
    </thead>
    <tbody>
      <#list beansMap["beanList"] as bean>
      <tr>
        <#list beansMap["beanFields"] as field>
          <#if field["name"]=="authorities">
          <#elseif field["name"]=="encoder">
          <#else>
              <td>${bean[field["name"]]!"null"}</td>
          </#if>
        </#list>
          <td>
              <a class="mvc-button" href="/user/delete/${bean.id}">delete</a>
              ||
              <a class="mvc-button" href="/user/edit/${bean.id}">edit</a>
          </td>
      </tr>
      </#list>


    </tbody>
</table>
<p>${beansMap["beanList"]?size} users found</p>
</#if>



<#include 'common/footer.ftl'>