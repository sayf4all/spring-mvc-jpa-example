<#include 'common/header.ftl'>


<table>
    <caption><@spring.message 'item.overview.page.title'/></caption>
    <thead>
    <tr>
    <#list beansMap["beanFields"] as field>
      <#if field["name"]=="fileList">
      <#elseif field["name"]=="date">
      <#else>
          <td>${field["name"]}</td>
      </#if>
    </#list>
        <td>actie</td>
    </tr>
    </thead>
    <tbody>
    <#list beansMap["beanList"] as bean>
    <tr>
      <#list beansMap["beanFields"] as field>
        <#if field["name"]=="fileList">
            <!--  excluden -->
        <#elseif field["name"]=="date">
            <!--  excluden -->
        <#elseif field["name"]=="category">
            <td><#if bean[field["name"]]??>${bean[field["name"]].name}</#if></td>
        <#else>
            <td>${bean[field["name"]]!""}</td>
        </#if>
      </#list>
        <td>
            <a class="mvc-button" href="#" onclick="deleteItem(${bean.id})">delete</a>
            ||
            <a class="mvc-button" href="/item/edit/${bean.id}">edit</a>
        </td>
    </tr>
    </#list>
    </tbody>
</table>
<a class="mvc-button" id="newItemButton">edit</a>


<#include 'common/footer.ftl'>
