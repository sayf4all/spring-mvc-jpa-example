<div class="mvc-nav">
    <div class="mvc-nav-l"></div>
    <div class="mvc-nav-r"></div>
    <div class="mvc-nav-outer">
        <ul class="mvc-hmenu">
            <li>
                <a href="/category/overview" class="active"><span class="l">
                    </span><span class="r"></span><span class="t"><@spring.message 'category.title.plural'/></span>
                </a>
                <ul>
                    <li>
                        <a href="/category/new"></span><span class="r"></span><span class="t">toevoegen</span></a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="/item/overview">
                    <span class="l"></span><span class="r"></span><span
                        class="t"><@spring.message 'item.overview.page.title'/></span>
                </a>
                <ul>
                    <li>
                        <a href="/item/new"></span><span class="r"></span><span class="t">toevoegen</span></a>
                    </li>
                </ul>
            </li>

            <li>
                <a href="/contact">
                    <span class="l"></span><span class="r"></span><span
                        class="t"><@spring.message 'contact.page.title'/></span>
                </a>
            </li>


        </ul>
    </div>
</div>



