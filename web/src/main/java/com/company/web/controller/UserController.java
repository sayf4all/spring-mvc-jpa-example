package com.company.web.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import com.company.data.model.users.User;
import com.company.service.impl.FileServiceImpl;
import com.company.service.impl.UserServiceImpl;
import com.company.web.controller.util.Paths;


@Controller
public class UserController extends AbstractController {

  // templates
  public static final String TEMPLATE_USER_OVERVIEW = "overview_user";
  public static final String TEMPLATE_USER_NEW_FORM = "add_user";
  public static final String TEMPLATE_USER_PROFILE = "profile";
  public static final String TEMPLATE_USER_SHOW_AND_EDIT_FORM = "new_user";
  public static final String USER_OVERVIEW = "user overview";
  private static final Logger LOG = LoggerFactory.getLogger(UserController.class);
  @Autowired
  UserServiceImpl userService;
  @Autowired
  FileServiceImpl fileService;


  @InitBinder
  public void initBinder(WebDataBinder binder, HttpServletRequest request) {
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
    dateFormat.setLenient(false);
    binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, false));
  }


  @ModelAttribute("user")
  public User getUser(HttpServletRequest request) {
    return new User();
  }


  @RequestMapping(value = Paths.USER_ADD, method = RequestMethod.POST)
  public String addUser(@ModelAttribute("user") @Valid User user, BindingResult bindingResult, Model model,
      @RequestParam("firstName") String firstName, @RequestParam("lastName") String lastName,
      @RequestParam("username") String userName, @RequestParam("password") String password,
      @RequestParam("roleType") String roleType

  ) {

    LOG.info(
        "nieuwe user om toe te voegen, firstname:" + firstName + " lastname:" + lastName + " username:" + userName + " roletype:"
            + roleType);
    if (bindingResult.hasErrors()) {
      setPageAttributes(model, USER_OVERVIEW);
      addItemFormBindingErrors(bindingResult, model);
      return TEMPLATE_USER_NEW_FORM;
    }
    else {
      userService.add(user);
    }
    return "redirect:/user/overview";
  }


  @RequestMapping(value = Paths.USER_NEW)
  public String create(Model model, HttpServletRequest request) {
    String pageTitel = "new user";
    setPageAttributes(model, pageTitel);
    model.addAttribute("action", "new");

    if (request.getParameter("userCreated") != null && request.getParameter("userCreated").equals("true")) {
      model.addAttribute("userCreated", "true");
    }
    return TEMPLATE_USER_NEW_FORM;
  }


  @RequestMapping(value = Paths.USER_PROFILE)
  public String profile(Model model) {
    String pageTitel = "user profile";
    UserDetails userDetails = getCurrentUserDetails();
    User user = userService.getUserByUserName(userDetails.getUsername());

    model.addAttribute("id", user.getId());
    model.addAttribute("username", user.getUsername());
    model.addAttribute("firstname", user.getFirstName());
    model.addAttribute("lastname", user.getLastName());
    model.addAttribute("fileList", fileService.listAll());

    Long profileImageId = null;

    model.addAttribute("action", "viewProfile");
    setPageAttributes(model, pageTitel);
    return TEMPLATE_USER_PROFILE;
  }


  @RequestMapping(value = Paths.USER_DELETE_USERNAME, method = RequestMethod.GET)
  @ResponseBody
  public String deleteUser(@PathVariable(value = "username") String userName) {
    LOG.info("User om te verwijderen met userName:" + userName);
    JSONObject results = new JSONObject();
    results.put("success", userService.deleteUser(userName));
    return results.toJSONString();
  }


  @RequestMapping(value = Paths.USER_EDIT_USERNAME, method = RequestMethod.GET)
  public String editUser(Model model, @PathVariable(value = "username") String userName) {
    String pageTitel = "edit user";
    setPageAttributes(model, pageTitel);
    LOG.info("User om te editen met userName:" + userName);

    if (userName != null) {
      User user = userService.getUserByUserName(userName);

      if (user != null) {
        model.addAttribute("id", user.getId());
        model.addAttribute("username", user.getUsername());
        model.addAttribute("firstname", user.getFirstName());
        model.addAttribute("lastname", user.getLastName());
        model.addAttribute("password", "********");
        model.addAttribute("action", "edit");
        return TEMPLATE_USER_SHOW_AND_EDIT_FORM;
      }
    }
    return TEMPLATE_USER_OVERVIEW;
  }


  @RequestMapping(value = Paths.USER_OVERVIEW)
  public String getUserOverVieuw(Model model) {
    setPageAttributes(model, USER_OVERVIEW);
    List<User> userList = userService.getUserOverviewList();
    if (userList.size() > 0) {
      model.addAttribute("beansMap", getBeansMapping(userList));
      model.addAttribute("userList", userList);
    }

    return TEMPLATE_USER_OVERVIEW;
  }
}
