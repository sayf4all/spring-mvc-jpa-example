package com.company.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.company.data.model.items.Category;


@Service
@Transactional
public interface CategoryService extends CrudInterface<Category, Long> {
}
