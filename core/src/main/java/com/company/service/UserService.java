package com.company.service;

import com.company.data.model.users.User;


public interface UserService extends CrudInterface<User, Long> {

  public User addUser(String firstName, String lastName, String userName, String password, String roleType);


  public boolean deleteUser(String userName);


  public User getUserByUserName(String userName);

}
