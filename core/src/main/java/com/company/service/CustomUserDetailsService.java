package com.company.service;

import java.util.Collection;
import java.util.List;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
@Transactional(readOnly = true)
public interface CustomUserDetailsService extends UserDetailsService {

  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException;


  public User getUserDetailsObject(com.company.data.model.users.User user);


  public Collection<? extends GrantedAuthority> grantAuthorities(Long role);


  public List<String> getRoles(Long role);


  public List<GrantedAuthority> getGrantedAuthorities(List<String> roles);

}
