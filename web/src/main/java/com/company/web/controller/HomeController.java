package com.company.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import com.company.service.CategoryService;


@Controller
public class HomeController extends AbstractController {
  //templates
  public static final String TEMPLATE_HOME = "index";
  private static final Logger LOG = LoggerFactory.getLogger(HomeController.class);
  @Autowired
  private ResourceLoader resourceLoader;

  @Autowired
  private CategoryService categoryService;


  @RequestMapping(value = "/")
  public String showRevision(Model model) {
    String pageTitle = "home";
    setPageAttributes(model, pageTitle);
    return TEMPLATE_HOME;
  }

}
