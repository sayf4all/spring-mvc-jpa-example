<#include 'common/header.ftl'>

<p>
<div id="contact-block" class="art-post-inner art-article">
    Contatgegevens:
    <ul>
        <li>
            <div><@spring.message 'contact.location'/></div>
            <div><@spring.message 'contact.address'/></div>
            <div><@spring.message 'contact.telephone'/></div>
            <div><@spring.message 'contact.email'/></div>
        </li>
    </ul>
</div></p>


<#include 'common/footer.ftl'>