package com.company.data.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import com.company.data.model.users.Authority;


public interface AuthorityRepository extends JpaRepository<Authority, Long> {

}
