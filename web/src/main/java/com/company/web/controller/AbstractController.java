package com.company.web.controller;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.TypeMismatchException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.NoSuchRequestHandlingMethodException;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.support.RequestContextUtils;
import com.company.service.CategoryService;
import com.company.web.controller.util.Paths;


public abstract class AbstractController {
  public static final String ACCESS_IS_DENIED = "access is denied";
  public static final String DATA_ACCESS_FAILED = "data access failed";
  // Att
  public static final String LOGGED_IN = "loggedIn";
  public static final String USER_NAME = "userName";
  public static final String PAGETITEL = "pagetitel";
  public static final String AUTHORISATION_LEVELS = "authorisationLevels";
  public static final String CATEGORIES = "categories";
  private static final Logger LOG = LoggerFactory.getLogger(AbstractController.class);
  private static final String MESSAGES = "messages";
  private static final String ERROR_MESSAGES = "errors";
  protected Map<String, String> urls = new HashMap<String, String>();

  @Autowired
  CategoryService categoryService;


  // -- Utility methods
  public static String redirect(String url) {
    return String.format(Paths.REDIRECT, url);
  }


  public static String forward(String url) {
    return String.format("forward:%s", url);
  }


  public static boolean hasErrors(HttpServletRequest request) {
    Map<String, ?> inputFlashMap = RequestContextUtils.getInputFlashMap(request);
    return inputFlashMap != null && inputFlashMap.containsKey(ERROR_MESSAGES);
  }


  @SuppressWarnings("unchecked")
  public static List<String> getErrors(HttpServletRequest request) {
    Map<String, ?> inputFlashMap = RequestContextUtils.getInputFlashMap(request);
    return (List<String>) inputFlashMap.get(ERROR_MESSAGES);
  }


  public static boolean hasMessages(HttpServletRequest request) {
    Map<String, ?> inputFlashMap = RequestContextUtils.getInputFlashMap(request);
    return inputFlashMap != null && inputFlashMap.containsKey(MESSAGES);
  }


  @SuppressWarnings("unchecked")
  public static List<String> getMessages(HttpServletRequest request) {
    Map<String, ?> inputFlashMap = RequestContextUtils.getInputFlashMap(request);
    return (List<String>) inputFlashMap.get(MESSAGES);
  }


  public static boolean hasErrors(HttpSession session) {
    return session.getAttribute(ERROR_MESSAGES) != null;
  }


  public static Object getErrors(HttpSession session) {
    return session.getAttribute(ERROR_MESSAGES);
  }


  public static boolean hasMessages(HttpSession session) {
    return session.getAttribute(MESSAGES) != null;
  }


  public static Object getMessages(HttpSession session) {
    return session.getAttribute(MESSAGES);
  }


  @SuppressWarnings("unchecked")
  public static void addError(Model model, String errorMessage) {
    if (!model.containsAttribute(ERROR_MESSAGES)) {
      model.addAttribute(ERROR_MESSAGES, new ArrayList<String>());
    }
    List<String> errors = (List<String>) model.asMap().get(ERROR_MESSAGES);
    errors.add(errorMessage);
  }


  @SuppressWarnings("unchecked")
  public static void addMessage(Model model, String message) {
    if (!model.containsAttribute(MESSAGES)) {
      model.addAttribute(MESSAGES, new ArrayList<String>());
    }
    List<String> messages = (List<String>) model.asMap().get(MESSAGES);
    messages.add(message);
  }


  @SuppressWarnings("unchecked")
  public static void addMessage(RedirectAttributes redirectAttributes, String successMessage) {
    List<String> messages = ((List<String>) redirectAttributes.getFlashAttributes().get(MESSAGES));
    if (messages == null) {
      messages = new ArrayList<String>();
      redirectAttributes.addFlashAttribute(MESSAGES, messages);
    }
    messages.add(successMessage);
  }


  public void setPageAttributes(Model model, String pageTitel) {

    model.addAttribute(CATEGORIES, categoryService.getAll());
    model.addAttribute(LOGGED_IN, true);
    model.addAttribute(PAGETITEL, pageTitel);
  }


  public void setPageAttributes(ModelAndView modelAndView, String pageTitel) {
    //UserDetails userDetails = getCurrentUserDetails();
    //if (userDetails != null) {
    modelAndView.getModelMap().put(CATEGORIES, categoryService.getAll());
    modelAndView.getModelMap().put(PAGETITEL, pageTitel);
    //model.addAttribute(AUTHORISATION_LEVELS, userDetails.getAuthorities());
    //}
  }


  public Map getBeansMapping(List list) {
    if (null != list && list.size() > 0) {
      Field[] beanFields = getBeanAttributes((list.get(0)).getClass());
      Map map = new HashMap();
      map.put("beanFields", beanFields);
      map.put("beanList", list);
      return map;
    }
    throw new IllegalArgumentException("Geen items gevonden");
  }


  public String redirectToReferer(HttpServletRequest request) {
    String referer = request.getHeader("Referer");
    return "redirect:" + referer;
  }


  private void tryToAddUserDetails(ModelAndView mav) {
    UserDetails userDetails = getCurrentUserDetails();
    if (userDetails != null) {
      mav.getModel().put(LOGGED_IN, true);
      mav.getModel().put(USER_NAME, userDetails.getUsername());
    }
  }


  public UserDetails getCurrentUserDetails() {
    SecurityContext securityContext = SecurityContextHolder.getContext();
    Authentication authentication = securityContext.getAuthentication();
    if (authentication != null) {
      Object principal = authentication.getPrincipal();
      return principal instanceof UserDetails ? (UserDetails) principal : null;
    }
    return null;
  }


  public Field[] getBeanAttributes(java.lang.Class clazz) {
    return clazz.getDeclaredFields();
  }


  @ExceptionHandler(Exception.class)
  public ModelAndView handleExceptions(Exception anExc) {
    ModelAndView mav = new ModelAndView();

    if (anExc instanceof DataAccessException) {
      LOG.warn(DATA_ACCESS_FAILED);
      mav.setViewName("http/500");
    }
    else if (anExc instanceof NoSuchRequestHandlingMethodException) {
      LOG.warn("NoSuchRequestHandlingMethodException exception");
      mav.setViewName("http/404");
    }
    else if (anExc instanceof TypeMismatchException) {
      LOG.warn("TypeMismatchException exception");
      mav.setViewName("http/500");
    }
    else if (anExc instanceof MissingServletRequestParameterException) {
      LOG.warn("MissingServletRequestParameterException exception", anExc);
      mav.setViewName("http/404");
    }
    else if (anExc instanceof NoSuchRequestHandlingMethodException) {
      LOG.warn("NumberFormatException exception");
      mav.setViewName("http/500");
    }
    else if (anExc instanceof IllegalArgumentException) {
      LOG.warn("No items found exception");
      if (anExc.getMessage().equals("Geen items gevonden")) {
        mav.setViewName("empty_item");
        mav.getModel().put(CATEGORIES, categoryService.getAll());
      }
      setPageAttributes(mav, "Overview");
    }
    else {
      LOG.error("uncaughtException exception!: " + anExc.getMessage() + "\n\r" + anExc.getStackTrace());
      List<String> errors = new ArrayList<String>();
      errors.add("Unknown exception! " + anExc.getMessage());
      Map<String, Object> model = new HashMap<String, Object>();
      model.put("errors", errors);
      mav.getModel().putAll(model);
      mav.getView();
      mav.setViewName("http/500");
    }
    return mav;
  }

  public void addItemFormBindingErrors(BindingResult bindingResult, Model model) {
    for (FieldError fieldError : bindingResult.getFieldErrors()) {
      addError(model, fieldError.getField());
    }
  }
}
