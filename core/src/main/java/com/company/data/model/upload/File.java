package com.company.data.model.upload;

import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
@Entity
public class File {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;
  private String filename;
  private String notes;
  private String type;
  private String filePath;

  @GeneratedValue(strategy = GenerationType.AUTO)
  @Temporal(TemporalType.TIMESTAMP)
  private Date timeStamp;

}