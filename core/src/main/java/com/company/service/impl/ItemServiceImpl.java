package com.company.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.company.data.model.items.Category;
import com.company.data.model.items.Item;
import com.company.data.repository.ItemRepository;
import com.company.service.ItemService;


@Service
@Transactional(readOnly = true)
public class ItemServiceImpl implements ItemService {
  private static final Logger LOG = LoggerFactory.getLogger(ItemServiceImpl.class);

  @Autowired
  private ItemRepository itemRepository;

  @Autowired
  private CategoryServiceImpl categoryService;


  @Override
  public Item add(Item object) {
    Item savedItem = null;
    try {
      savedItem = itemRepository.saveAndFlush(object);
    }
    catch (Exception ex) {
      LOG.error("Item could not be saved due to an error");
    }
    return savedItem;
  }


  @Override
  public Item get(Long id) {
    Item item = null;
    try {
      item = itemRepository.findById(id);
    }
    catch (Exception ex) {
      LOG.warn("Error bij het ophalen van ");
    }
    if (item != null) {
      LOG.info("Item gevonden id:" + item.getId() + " naam:" + item.getName() + " price:" + item.getPrice() + " beschrijving:"
          + item.getShortDescription());
      return item;
    }
    else {
      LOG.error("Item niet gevonden!");
      return null;
    }
  }


  @Override
  public List<Item> getAll() {
    return itemRepository.findAll();
  }


  @Override
  public Item update(Item object) {
    return null; // To change body of implemented methods use File | Settings | File Templates.
  }


  @Override
  public Boolean delete(Item item) {
    try {
      item = itemRepository.findById(item.getId());
      itemRepository.delete(item.getId());
      itemRepository.flush();

      if (itemRepository.findById(item.getId()) == null) {

        LOG.info("Item verwijderd met id: " + item.getId());
        return true;
      }
    }
    catch (Exception ex) {
      LOG.info("Onbekende fout is opgetreden\n\r" + ex.getMessage());
    }

    return false;
  }


  @Override
  public List<Item> getByCategory(Category category) {
    return itemRepository.findByCategory(category);
  }


  @Override
  public Item add(String name, String price, String description, String fullDescription, String city, String street,
      String categoryId) {
    Category category = categoryService.get(Long.parseLong(categoryId));
    Item item = new Item();
    item.setName(name);
    item.setPrice(Float.parseFloat(price));
    item.setShortDescription(description);
    item.setCategory(category);
    item.setCity(city);
    item.setStreet(street);
    item.setFullDescription(fullDescription);
    return add(item);
  }
}
