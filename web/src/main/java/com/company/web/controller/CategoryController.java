package com.company.web.controller;

import java.util.List;
import java.util.Map;

import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import com.company.data.model.items.Category;
import com.company.service.CategoryService;
import com.company.web.controller.util.Paths;


@Controller
public class CategoryController extends AbstractController {
  // templates
  public static final String CATEGORY_OVERVIEW = "overview_category";
  public static final String CATEGORY_NEW = "add_category";
  public static final String CATEGORY_EDIT = "edit_category";
  private static final Logger LOG = LoggerFactory.getLogger(CategoryController.class);
  @Autowired
  CategoryService categoryService;
  private String pageTitle = "Category";


  @RequestMapping(value = Paths.CATEGORY_OVERVIEW)
  public String getCategoryOverViewList(Model model) {
    setPageAttributes(model, pageTitle);
    Map categories = getBeansMap();
    model.addAttribute("beansMap", categories);
    return CATEGORY_OVERVIEW;
  }


  private Map getBeansMap() {
    List<Category> categoryList = categoryService.getAll();
    return getBeansMapping(categoryList);
  }


  @RequestMapping(value = Paths.CATEGORY_NEW)
  public String newCategory(Model model) {
    String pageTitle = "new";
    setPageAttributes(model, pageTitle);
    return CATEGORY_NEW;
  }


  @RequestMapping(value = Paths.CATEGORY_UPDATE)
  public String addCategory(@RequestParam(value = "categoryId", required = false) Long categoryId,
      @RequestParam(value = "categoryName", required = false) String categoryName) {

    if (null != categoryId && null != categoryName) {
      Category category = categoryService.get(categoryId);
      category.setName(categoryName);
      category = categoryService.update(category);
      if (null != category) {
        LOG.info("category geupdate met naam:" + category.getName());
      }
    }
    return redirect(Paths.CATEGORY_OVERVIEW);
  }


  @RequestMapping(value = Paths.CATEGORY_ADD)
  public String addCategory(@RequestParam(value = "categoryName", required = false) String categoryName) {
    if (null != categoryName) {
      Category category = new Category();
      category.setName(categoryName);
      category = categoryService.add(category);
      if (null != category) {
        LOG.info("category toegevoegd met naam:" + category.getName());
      }
    }
    return redirect(Paths.CATEGORY_OVERVIEW);
  }


  @RequestMapping(value = Paths.CATEGORY_EDIT)
  public String getCategory(@PathVariable(value = "id") Long id, Model model) {
    String pageTitle = "Edit Category";
    setPageAttributes(model, pageTitle);
    model.addAttribute("category", categoryService.get(id));
    return CATEGORY_EDIT;
  }


  @RequestMapping(value = Paths.CATEGORY_DELETE, method = RequestMethod.GET)
  @ResponseBody
  public JSONObject deleteItem(@PathVariable(value = "id") Long id) {
    LOG.info("item om te verwijderen met id:" + id);
    JSONObject results = new JSONObject();
    results.put("success", categoryService.delete(categoryService.get(id)));
    return results;
  }
}
