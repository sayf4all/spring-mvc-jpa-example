<div class="mvc-layout-cell mvc-sidebar1">
    <div class="mvc-vmenublock">
        <div class="mvc-vmenublock-body">
            <div class="mvc-vmenublockcontent">
                <div class="mvc-vmenublockcontent-body">
                    <p>Ladi Dadi Daaaaa</p>

                    <div class="cleared"></div>
                </div>
            </div>
            <div class="cleared"></div>
        </div>
    </div>
    <div class="mvc-block">
        <div class="mvc-block-body">
            <div class="mvc-blockcontent">
                <div class="mvc-blockcontent-body">
                    <ul class="mvc-vmenu">
                    <#if categories??>
                      <#list categories as category>
                          <li>
                              <a href="/item/get/category/${category.id}">${category.name}</a>
                          </li>
                      </#list>
                    </#if>
                    </ul>
                    <div class="cleared"></div>
                </div>
            </div>
            <div class="cleared"></div>
        </div>
    </div>

    <div class="cleared"></div>
</div>