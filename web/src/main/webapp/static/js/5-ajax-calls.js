/*function submitNewItem(id) {
 var name = $('#itemName').val();
 var priceInput = $('#priceInput').val();
 var description = $('#description').val();
 var categoryId = $('#categoryId').val();
 if (name != undefined && priceInput != undefined && description != undefined) {
 var url = "/item/add/" + name + "/" + priceInput + "/" + description;

 $.ajax({
 type: "GET",
 url: url
 }).done(function (data) {
 var resultObject = JSON.parse(data);
 if (resultObject['success'] == true) {
 var feedBackMessage = 'Item is aangemaakt.<br>';
 var href = "/item/new";
 var question = false;
 showPopUp(feedBackMessage, href, question);

 } else if (resultObject['success'] == false) {
 var feedBackMessage = 'Item aanmaken niet gelukt';
 var href = $(this).attr('href');
 var question = false;

 showPopUp(feedBackMessage, href, question);
 e.preventDefault();

 }
 });
 }
 }

 $("#ajaxSaveButton").click(function () {
 var request;
 var $form = $('#new_category_form');
 var $inputs = $form.find("input, select, button, textarea");
 var serializedData = $form.serialize();
 $inputs.prop("disabled", true);

 $.ajax({
 url: "/category/add",
 type: "post",
 data: serializedData
 }).done(function (response, textStatus, jqXHR) {
 // log a message to the console
 var resultObject = JSON.parse(response);
 var feedBackMessage = '';
 if (resultObject['success'] === false) {
 feedBackMessage = 'categorie aanmaken is niet gelukt';
 } else if (resultObject['success'] === true) {
 feedBackMessage = 'categorie aanmaken is gelukt';
 }
 var href = $(this).attr('href');
 var question = false;

 showPopUp(feedBackMessage, href, question);
 }).fail(function (jqXHR, textStatus, errorThrown) {
 console.error("The following error occured: " + textStatus,
 errorThrown);
 }).always(function () {
 $inputs.prop("disabled", false);
 });
 }
 ); */


function submitCategoryForm(url, form) {
    $("#new_category_form").click(function () {
        form.submit();
    });
}

/*
 function deleteItem(id) {

 var url = "/item/delete/" + id;
 if (id != undefined) {
 $.ajax({
 type: "GET",
 url: url
 })
 .done(
 function (data) {
 var resultObject = JSON.parse(data);
 if (resultObject['success'] == true) {
 var feedBackMessage = 'Item is verwijderd.<br> Wilt u terug naar het overzicht?';
 var href = "/item/overview";
 var question = true;
 showPopUp(feedBackMessage, href, question);
 } else if (resultObject['success'] == false) {
 var feedBackMessage = 'Item verwijderen is niet gelukt';
 var href = $(this).attr('href');
 var question = false;

 showPopUp(feedBackMessage, href, question);
 e.preventDefault();
 }
 });
 }


 }

 function deleteUser(name) {

 var url = "/user/delete/" + name;
 $.ajax({
 type: "GET",
 url: url
 }).done(
 function (data) {
 var resultObject = JSON.parse(data);
 if (resultObject['success'] == true) {
 var feedBackMessage = 'User is verwijderd.<br> Wilt u terug naar het overzicht?';
 var href = "/user/overview";
 var question = true;
 showPopUp(feedBackMessage, href, question);
 } else if (resultObject['success'] == false) {
 var feedBackMessage = 'user verwijderen is niet gelukt';
 var href = $(this).attr('href');
 var question = false;

 showPopUp(feedBackMessage, href, question);
 e.preventDefault();
 }
 });

 }

 function uploadProfileImage(id) {
 var url = "/user/setprofileimage/" + id;
 if (id != undefined) {
 $.ajax({
 type: "GET",
 url: url
 }).done(function (data) {
 var resultObject = JSON.parse(data);
 if (resultObject['success'] == true) {
 window.location = '/user/profile';
 } else if (resultObject['success'] == false) {
 var feedBackMessage = 'Item verwijderen is niet gelukt';
 var href = $(this).attr('href');
 var question = false;

 showPopUp(feedBackMessage, href, question);
 e.preventDefault();
 }
 });
 }

 }

 function addNewUser() {
 var request;
 $("#new_user_form").submit(
 function (event) {
 if (request) {
 request.abort();
 }
 var $form = $(this);
 var $inputs = $form.find("input, select, button, textarea");
 var serializedData = $form.serialize();
 $inputs.prop("disabled", true);

 request = $.ajax({
 url: "/user/add",
 type: "post",
 data: serializedData
 });

 request.done(function (response, textStatus, jqXHR) {
 // log a message to the console
 var resultObject = JSON.parse(response);
 var feedBackMessage = '';
 if (resultObject['success'] === false) {
 feedBackMessage = 'User aanmaken is niet gelukt';
 } else if (resultObject['success'] === true) {
 feedBackMessage = 'User aanmaken is gelukt';
 }
 var href = $(this).attr('href');
 var question = false;

 showPopUp(feedBackMessage, href, question);
 });

 request.fail(function (jqXHR, textStatus, errorThrown) {
 console.error("The following error occured: " + textStatus,
 errorThrown);
 });

 request.always(function () {
 $inputs.prop("disabled", false);
 });

 event.preventDefault();
 });

 }

 function updateUser() {

 var request;
 $("#update_user_form").submit(
 function (event) {
 if (request) {
 request.abort();
 }
 var $form = $(this);
 var $inputs = $form.find("input, select, button, textarea");
 var serializedData = $form.serialize();
 $inputs.prop("disabled", true);

 request = $.ajax({
 url: "/user/add",
 type: "post",
 data: serializedData
 });

 request.done(function (response, textStatus, jqXHR) {
 var resultObject = JSON.parse(response);
 var feedBackMessage = '';
 if (resultObject['success'] === false) {
 feedBackMessage = 'User aanmaken is niet gelukt';
 } else if (resultObject['success'] === true) {
 feedBackMessage = 'User aanmaken is gelukt';
 }
 var href = $(this).attr('href');
 var question = false;

 showPopUp(feedBackMessage, href, question);
 });

 request.fail(function (jqXHR, textStatus, errorThrown) {
 console.error("The following error occured: " + textStatus,
 errorThrown);
 });

 request.always(function () {
 $inputs.prop("disabled", false);
 });

 event.preventDefault();
 });
 }
 */

function deleteItem(id) {

    var url = "/item/delete/" + id;
    if (id != undefined) {
        $.ajax({
            type: "GET",
            url: url
        }).done(
            function (data) {
                var resultObject = data;

                if (resultObject['success'] == true) {
                    var feedBackMessage = 'Item is verwijderd.<br> Wilt u terug naar het overzicht?';
                    var href = "/item/overview";
                    alert(feedBackMessage);
                    location.reload();
                } else if (resultObject['success'] == false) {
                    var feedBackMessage = 'Item verwijderen is niet gelukt';
                    var href = $(this).attr('href');
                    var question = false;
                    alert(feedBackMessage);
                    e.preventDefault();
                }
            });
    }


}