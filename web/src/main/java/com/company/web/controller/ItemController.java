package com.company.web.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import com.company.data.model.items.Category;
import com.company.data.model.items.Item;
import com.company.service.impl.CategoryServiceImpl;
import com.company.service.impl.ItemServiceImpl;
import com.company.web.controller.util.Paths;


@Controller
public class ItemController extends AbstractController {
  // Parameters
  public static final String ID_PARAM = "id";
  // Attributen
  public static final String SUCCESS_ATTR = "success";
  // Templates
  public static final String ITEM_OVERVIEW_TEMPLATE = "overview_item";
  public static final String ITEM_NEW_FORM_TEMPLATE = "add_item";
  public static final String ITEM_EDIT_FORM_TEMPLATE = "edit_item";
  public static final String PAGE_TITLE = "Items";
  private static final Logger LOG = LoggerFactory.getLogger(ItemController.class);
  @Autowired
  CategoryServiceImpl categoryServiceImpl;
  @Autowired
  ItemServiceImpl itemService;


  @InitBinder
  public void initBinder(WebDataBinder binder, HttpServletRequest request) {
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
    dateFormat.setLenient(false);
    binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, false));
  }


  @ModelAttribute("item")
  public Item getItem(HttpServletRequest request) {
    return new Item();
  }


  @RequestMapping(value = Paths.ITEM_ADD_NAME_PRICE_DESCRIPTION, method = RequestMethod.POST)
  public String addItem(@ModelAttribute("item") @Valid Item item, BindingResult bindingResult, Model model,
      HttpServletRequest request, @RequestParam("categoryId") Long categoryId) {
    if (bindingResult.hasErrors()) {
      setPageAttributes(model, PAGE_TITLE);
      addItemFormBindingErrors(bindingResult, model);
      return ITEM_EDIT_FORM_TEMPLATE;
    }
    else {
      if (categoryId != null) {
        item.setCategory(categoryService.get(categoryId));
      }
      itemService.add(item);
    }
    return Paths.REDIRECT_TO_ITEM_OVERVIEW;
  }


  @RequestMapping(value = Paths.ITEM_NEW)
  public String create(Model model) {
    model.addAttribute("categories", categoryServiceImpl.getAll());
    return ITEM_NEW_FORM_TEMPLATE;
  }


  @RequestMapping(value = Paths.ITEM_DELETE_ID, method = RequestMethod.GET)
  @ResponseBody
  public JSONObject deleteItem(@PathVariable(value = "id") Long id) {
    LOG.info("item om te verwijderen met id:" + id);
    JSONObject results = new JSONObject();
    results.put(SUCCESS_ATTR, itemService.delete(itemService.get(id)));
    return results;
  }


  @RequestMapping(value = Paths.ITEM_EDIT, method = RequestMethod.GET)
  public String editItem(Model model, @PathVariable(value = ID_PARAM) Long id) {
    LOG.info("item om te editen met id:" + id);
    setPageAttributes(model, PAGE_TITLE);
    if (id != null) {
      Item item = itemService.get(id);
      if (item != null) {
        model.addAttribute(item);
        return ITEM_EDIT_FORM_TEMPLATE;
      }
    }
    addError(model, "Invalid or missing Item ID!");
    return ITEM_OVERVIEW_TEMPLATE;
  }


  @RequestMapping(value = Paths.ITEM_OVERVIEW)
  public String getItemOverVieuw(Model model) {
    setPageAttributes(model, PAGE_TITLE);
    Map items = getBeanMap(itemService.getAll());
    model.addAttribute("beansMap", items);
    return ITEM_OVERVIEW_TEMPLATE;
  }


  @RequestMapping(value = Paths.ITEM_OVERVIEW_BY_CATEGORY)
  public String getItemOverVieuw(Model model, @PathVariable(value = ID_PARAM) String categoryId) {
    setPageAttributes(model, PAGE_TITLE);
    Category category = categoryService.get(Long.parseLong(categoryId));
    Map items = getBeanMap(itemService.getByCategory(category));
    model.addAttribute("beansMap", items);
    return ITEM_OVERVIEW_TEMPLATE;
  }


  @RequestMapping(value = "/julius/ajax")
  @ResponseBody
  public JSONObject getJuliusAjax(Model model) {
    setPageAttributes(model, "Julius Rules!");
    Item item = new Item();
    item.setName("Julius");
    item.setCity("Amersfoort");
    JSONObject response = new JSONObject();
    response.put("juliusItem", item);
    return response;
  }


  @RequestMapping(value = "/julius")
  @ResponseBody
  public String getJulius(Model model) {
    setPageAttributes(model, "Julius Rules!");
    model.addAttribute("juliusBean", "Julius specifieke string!");
    return "julius_rules";
  }


  private void createItem(Model model, String name, String price, String description, String fullDescription, String city,
      String street, String categoryId) {
    LOG.info("nieuwe item om toe te voegen, Naam:" + name + " /Prijs:" + price + " /beschrijving" + description);
    Item item = itemService.add(name, price, description, fullDescription, city, street, categoryId);
    addMessage(model, "itemCreated: " + Boolean.valueOf(item != null) + "");
  }


  private Map getBeanMap(List<Item> itemList) {
    return getBeansMapping(itemList);
  }
}
