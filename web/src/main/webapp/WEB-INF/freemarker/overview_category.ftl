<#include 'common/header.ftl'>


<table align="center" class="table table-curved">
    <caption><@spring.message 'category.title.plural'/></caption>
    <thead>
    <tr>
    <#list beansMap["beanFields"] as field>
        <td>${field["name"]}</td>
    </#list>
        <td>actie</td>
    </tr>
    </thead>
    <tbody>
    <#list beansMap["beanList"] as bean>
    <tr>
      <#list beansMap["beanFields"] as field>
          <td>${bean[field["name"]]!""}</td>
      </#list>
        <td>
            <a class="mvc-button" href="/category/delete/${bean.id!""}">delete</a>
            ||
            <a class="mvc-button" href="/category/edit/${bean.id!""}">edit</a>
        </td>
    </tr>
    </#list>
    </tbody>
</table>


<#include 'common/footer.ftl'>
