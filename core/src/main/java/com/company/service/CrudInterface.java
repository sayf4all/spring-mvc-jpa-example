package com.company.service;

import java.util.List;

import com.company.service.impl.NotFoundException;


public interface CrudInterface<T, ID> {

  public T add(T object);


  public T get(ID id) throws NotFoundException;


  public List<T> getAll();


  public T update(T object);


  public Boolean delete(T object);
}
