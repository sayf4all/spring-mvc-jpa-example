package com.company.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.company.data.model.items.Category;
import com.company.data.repository.CategoryRepository;
import com.company.service.CategoryService;


@Service
@Transactional
public class CategoryServiceImpl implements CategoryService {

  private static final Logger LOG = LoggerFactory.getLogger(CategoryServiceImpl.class);

  @Autowired
  private CategoryRepository categoryRepository;


  public Category add(Category category) {

    Category savedCategory = null;
    try {
      savedCategory = categoryRepository.saveAndFlush(category);
    }
    catch (Exception ex) {
      LOG.error("Category could not be saved due to an error");
    }

    if (savedCategory != null && savedCategory.getId() != null) {
      LOG.info("nieuwe Category aangemaakt " + savedCategory.getId() + "/" + category.getName() + "/" + category.getName());
      return savedCategory;
    }
    else {
      LOG.error("category niet aangemaakt!");
      return null;
    }
  }


  public Category get(Long categoryId) throws NotFoundException {
    Category category = categoryRepository.findOne(categoryId);
    if (category != null) {
      return category;
    }
    throw new NotFoundException("Fout! geen categorie gevonden");
  }


  public Category update(Category category) {
    Category savedCategory = categoryRepository.saveAndFlush(category);
    return savedCategory;
  }


  @Override
  public Boolean delete(Category category) {
    Category foundCategory = categoryRepository.findOne(category.getId());
    if (null != foundCategory) {
      try {
        categoryRepository.delete(foundCategory);
        categoryRepository.flush();

        if (categoryRepository.findOne(category.getId()) == null) {

          LOG.info("Category verwijderd met id: " + category.getId());
          return true;
        }
      }
      catch (Exception ex) {
        LOG.info("Onbekende fout is opgetreden\n\r" + ex.getMessage());
      }
    }
    return false;
  }


  @Override
  public List<Category> getAll() {
    List<Category> categoryList = null;
    try {
      return (categoryRepository.findAll());
    }
    catch (Exception ex) {
      LOG.error("Category could not find categories");
    }
    return categoryList;
  }
}
