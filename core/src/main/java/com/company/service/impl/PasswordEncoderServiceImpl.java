package com.company.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.security.authentication.encoding.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
@Transactional(readOnly = true)
public class PasswordEncoderServiceImpl implements PasswordEncoder {
  @Autowired
  CustomUserDetailsServiceImpl customUserDetailsService;


  public String getMD5EncodedPasswordHash(String pass) {
    try {
      PasswordEncoder encoder = new Md5PasswordEncoder();
      String hashedPass = encoder.encodePassword(pass, null);
      return hashedPass.toLowerCase();
    }
    catch (Exception e) {

    }
    return null;
  }


  @Override
  public String encodePassword(String rawPass, Object salt) {
    try {
      PasswordEncoder encoder = new Md5PasswordEncoder();
      String hashedPass = encoder.encodePassword(rawPass, null);
      return hashedPass.toLowerCase();
    }
    catch (Exception e) {

    }
    return null;
  }


  @Override
  public boolean isPasswordValid(String encPass, String rawPass, Object salt) {
    try {
      String hashedPass = this.encodePassword(rawPass, null);
      return hashedPass.toLowerCase().equals(encPass);
    }
    catch (Exception e) {

    }
    return false;
  }
}
