<#include 'common/header.ftl'>
<p>vul de itemgegevens hieronder in</p>

<form action="/item/add" method="post" class="appnitro" id="new_item_form">
    <ul>
        <li>
            <label for="name" class="description">Item </label>
            <input type="text" maxlength="255" class="element text medium" name="name" id="name">
        </li>
        <li>
            <label for="price" class="description">Price &euro;</label>
            <input type="text" size="10" class="element text currency" name="price" id="price">
        </li>
        <li>
            <label for="shortDescription" class="description">Short Decription </label>
            <textarea class="element textarea medium" name="shortDescription" id="shortDescription"></textarea>
        </li>
        <li>
            <label for="fullDescription" class="description">Full Decription </label>
            <textarea class="element textarea medium" name="fullDescription" id="fullDescription"></textarea>
        </li>
        <li>
            <label for="city" class="description">City </label>
            <textarea class="element textarea medium" name="city" id="city"></textarea>
        </li>
        <li>
            <label for="street" class="description">Street </label>
            <textarea class="element textarea medium" name="street" id="street"></textarea>
        </li>
        <li>
            <SELECT name="categoryId" id="category">
            <#list categories as category>
                <OPTION value="${category.id}">${category.name}</OPTION>
            </#list>
            </SELECT>
        </li>
    </ul>

    <input type="submit" value="Save" class="art-button" id="saveItem">
</form>
<a id="ajaxSaveButton" class="mvc-button">Ajax Save</a>


<div class="cleared"></div>


<#include 'common/footer.ftl'>





