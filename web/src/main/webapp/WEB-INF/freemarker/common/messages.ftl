<#if errors??>
<table class="mvc-blockcontent">
  <#list errors as error>
      <tr>
          <td>
              <h3 style="color: red">${error}</h3>
          </td>
      </tr>
  </#list>
</table>
</#if>

<#if messages??>
<table class="mvc-blockcontent">
  <#list messages as message>
      <tr>
          <td>
              <h3 style="color: black">${message}</h3>
          </td>
      </tr>
  </#list>
</table>
</#if>