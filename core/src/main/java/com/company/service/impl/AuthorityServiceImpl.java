package com.company.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.company.data.repository.AuthorityRepository;
import com.company.service.AuthorityService;


@Service
@Transactional(readOnly = true)
public class AuthorityServiceImpl implements AuthorityService {

  @Autowired
  AuthorityRepository authorityRepository;
}
