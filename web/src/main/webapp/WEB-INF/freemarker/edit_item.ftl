<#include 'common/header.ftl'>

<p>vul de itemgegevens hieronder in</p>

<form action="/item/add" method="post" class="appnitro" id="new_item_form">
    <ul>
        <li>
            <label for="name" class="description">Item </label>
            <input type="text" maxlength="255" class="element text medium" name="name" id="name"
                   value="${item.name}">
        </li>
        <li>
            <label for="price" class="description">Price &euro;</label>
            <span>
              <input type="text" size="10" class="element text currency" name="price" id="price"
                     value="${item.price!""}">
            </span>
        </li>
        <li>
            <label for="city" class="description">City</label>
            <input type="text" maxlength="255" class="element text medium" name="city" id="city"
                   value="${item.city!""}">
        </li>
        <li>
            <label for="street" class="description">Street</label>
            <input type="text" maxlength="255" class="element text medium" name="street" id="street"
                   value="${item.street!""}">
        </li>
        <li>
            <label for="shortDescription" class="description">Decription </label>
            <textarea class="element textarea medium" name="shortDescription" id="shortDescription">
            ${item.shortDescription!""}
            </textarea>
        </li>
        <li>
            <label for="fullDescription" class="description">fullDecription </label>
            <textarea class="element textarea medium" name="fullDescription"
                      id="fullDescription">${item.fullDescription!""}</textarea>
        </li>

        <li>
            <SELECT name="categoryId" id="category">
            <#list categories as category>
                <OPTION value="${category.id}">${category.name}</OPTION>
            </#list>
            </SELECT>
        </li>
    </ul>
    <input type="submit" value="Save" class="art-button" id="saveItem">
</form>



<#include 'common/footer.ftl'>





