package com.company.service.impl;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.company.data.model.upload.File;
import com.company.data.repository.FileRepository;
import com.company.service.FileService;


@Service
@Transactional(readOnly = true)
public class FileServiceImpl implements FileService {
  private static final Logger LOG = LoggerFactory.getLogger(FileServiceImpl.class);

  @Autowired
  FileRepository fileRepository;

  @Value("${upload.directory}")
  String uploadDirectory;


  public File find(Long id) {

    try {
      File file = fileRepository.findOne(id);

      return file;
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }

    return null;
  }


  public List<File> listAll() {
    try {
      List<File> files = fileRepository.findAll();
      return files;
    }
    catch (Exception e) {
      e.printStackTrace();
    }

    return null;
  }


  public boolean save(final File file) {
    try {
      File savedFile = fileRepository.saveAndFlush(file);
      if (savedFile != null) {
        return true;
      }
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
    return false;
  }


  public void delete(Long id) {
    try {
      fileRepository.delete(id);
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }


  public byte[] readFile(String file) throws IOException {
    return readFile(new java.io.File(file));
  }


  public byte[] readFile(java.io.File file) throws IOException {
    // Open file
    RandomAccessFile f = new RandomAccessFile(file, "r");
    try {
      // Get and check length
      long longlength = f.length();
      int length = (int) longlength;
      if (length != longlength) {
        throw new IOException("File size >= 2 GB");
      }
      // Read file and return data
      byte[] data = new byte[length];
      f.readFully(data);
      return data;
    }
    finally {
      f.close();
    }
  }

}
