package com.company.data.model.items;

import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;


@Getter
@Setter
@Entity(name = "item")
public class Item {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;

  @Temporal(TemporalType.TIMESTAMP)
  private Date date;

  @NotNull
  @Min(value = 0)
  @Max(value = 1 * 2 ^ 64)
  private Float price;

  @Length(min = 1, max = 64)
  private String name;

  @Length(min = 1, max = 64)
  private String city;

  @Length(min = 1, max = 64)
  private String street;

  @Length(min = 0, max = 4000)
  private String fullDescription;

  @Length(min = 1, max = 400)
  private String shortDescription;

  @OneToOne
  private Category category;

}
