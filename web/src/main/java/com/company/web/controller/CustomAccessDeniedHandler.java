package com.company.web.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandlerImpl;
import org.springframework.stereotype.Service;


@Service("accessDeniedHandler")
public class CustomAccessDeniedHandler extends AccessDeniedHandlerImpl {
  private static final Logger LOG = LoggerFactory.getLogger(CustomAccessDeniedHandler.class);


  @Override
  public void handle(HttpServletRequest request, HttpServletResponse response, AccessDeniedException exception)
      throws IOException, ServletException {
    LOG.info("############### Access Denied Handler!");
    setErrorPage("/accessDenied");
    super.handle(request, response, exception);
  }
}
