package com.company.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.company.data.model.items.Category;
import com.company.data.model.items.Item;


@Service
@Transactional(readOnly = true)
public interface ItemService extends CrudInterface<Item, Long> {

  public List<Item> getByCategory(Category category);


  public Item add(String name, String price, String description, String fullDescription, String city, String street,
      String categoryId);
}
