package com.company.data.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import com.company.data.model.items.Category;
import com.company.data.model.items.Item;


public interface ItemRepository extends JpaRepository<Item, Long> {

  List<Item> findByCategory(Category category);
}
