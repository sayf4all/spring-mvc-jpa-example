package com.company.data.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import com.company.data.model.users.User;


public interface UserRepository extends JpaRepository<User, Long> {

  User findByUsername(String username);


  User findById(Long id);


  List<User> findAll();

}
