package com.company.data.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import com.company.data.model.upload.File;


public interface FileRepository extends JpaRepository<File, Long> {

  List<File> findAll();
}
