package com.company.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.company.data.model.users.Authority;
import com.company.data.model.users.User;
import com.company.data.repository.UserRepository;
import com.company.service.UserService;


@Service
@Transactional(readOnly = true)
public class UserServiceImpl implements UserService {
  private static final Logger LOG = LoggerFactory.getLogger(UserServiceImpl.class);
  @Autowired
  UserRepository userRepository;

  @Autowired
  FileServiceImpl fileService;

  @Autowired
  PasswordEncoderServiceImpl passwordEncoderImpl;


  public User addUser(String firstName, String lastName, String userName, String password, String roleType) {

    User user;
    user = userRepository.findByUsername(userName.toLowerCase());

    if (user == null) {
      user = new User();
      user.setUsername(userName);
      user.setFirstName(firstName);
      user.setLastName(lastName);
      user.setPassword(passwordEncoderImpl.getMD5EncodedPasswordHash(password));
      Authority authority = new Authority();

      List<Authority> authorities = new ArrayList<Authority>();
      user.setAuthorities(authorities);
      user = this.add(user);

      return user;
    }
    return null;
  }


  public boolean deleteUser(String userName) {
    User user = userRepository.findByUsername(userName);
    if (user != null) {
      return delete(user);
    }
    return false;
  }


  public User getUserByUserName(String userName) {
    return userRepository.findByUsername(userName);
  }


  public List<User> getUserOverviewList() {
    return userRepository.findAll();
  }


  @Override
  public User add(User object) {
    return userRepository.saveAndFlush(object);
  }


  @Override
  public User get(Long id) {
    return userRepository.findById(id);
  }


  @Override
  public List<User> getAll() {
    return userRepository.findAll();
  }


  @Override
  public User update(User object) {
    return userRepository.saveAndFlush(object);
  }


  @Override
  public Boolean delete(User object) {
    userRepository.delete(object);
    userRepository.flush();
    return userRepository.findById(object.getId()) == null;
  }
}
