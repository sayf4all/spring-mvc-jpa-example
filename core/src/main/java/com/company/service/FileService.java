package com.company.service;

import java.io.IOException;
import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.company.data.model.upload.File;


@Service
@Transactional(readOnly = true)
public interface FileService {

  public File find(Long id);


  public List<File> listAll();


  public boolean save(final File file);


  public void delete(Long id);


  public byte[] readFile(String file) throws IOException;


  public byte[] readFile(java.io.File file) throws IOException;
}
