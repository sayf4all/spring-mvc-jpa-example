package com.company.service;

/**
 * Created with IntelliJ IDEA.
 * User: sayf.jawad
 * Date: 6-4-14
 * Time: 23:06
 * To change this template use File | Settings | File Templates.
 */
public class NotFoundException extends RuntimeException {

  public NotFoundException() {
    super();
  }


  public NotFoundException(String message) {
    super(message);
  }
}