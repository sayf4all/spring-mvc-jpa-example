/* begin Page */
/* Created by Artisteer v3.0.0.45570 */
// css hacks
(function ($) {
    // fix ie blinking
    var m = document.uniqueID && document.compatMode && !window.XMLHttpRequest && document.execCommand;
    try {
        if (!!m) {
            m('BackgroundImageCache', false, true);
        }
    }
    catch (oh) {
    }
    ;
    // css helper
    var data = [
        {str: navigator.userAgent, sub: 'Chrome', ver: 'Chrome', name: 'chrome'},
        {str: navigator.vendor, sub: 'Apple', ver: 'Version', name: 'safari'},
        {prop: window.opera, ver: 'Opera', name: 'opera'},
        {str: navigator.userAgent, sub: 'Firefox', ver: 'Firefox', name: 'firefox'},
        {str: navigator.userAgent, sub: 'MSIE', ver: 'MSIE', name: 'ie'}
    ];
    for (var n = 0; n < data.length; n++) {
        if ((data[n].str && (data[n].str.indexOf(data[n].sub) != -1)) || data[n].prop) {
            var v = function (s) {
                var i = s.indexOf(data[n].ver);
                return (i != -1) ? parseInt(s.substring(i + data[n].ver.length + 1)) : '';
            };
            $('html').addClass(data[n].name + ' ' + data[n].name + v(navigator.userAgent) || v(navigator.appVersion));
            break;
        }
    }
})(jQuery);

var _artStyleUrlCached = null;
function artGetStyleUrl() {
    if (null == _artStyleUrlCached) {
        var ns;
        _artStyleUrlCached = '';
        ns = jQuery('link');
        for (var i = 0; i < ns.length; i++) {
            var l = ns[i].href;
            if (l && /style\.ie6\.css(\?.*)?$/.test(l))
                return _artStyleUrlCached = l.replace(/style\.ie6\.css(\?.*)?$/, '');
        }
        ns = jQuery('style');
        for (var i = 0; i < ns.length; i++) {
            var matches = new RegExp('import\\s+"([^"]+\\/)style\\.ie6\\.css"').exec(ns[i].html());
            if (null != matches && matches.length > 0)
                return _artStyleUrlCached = matches[1];
        }
    }
    return _artStyleUrlCached;
}

function artFixPNG(element) {
    if (jQuery.browser.msie && parseInt(jQuery.browser.version) < 7) {
        var src;
        if (element.tagName == 'IMG') {
            if (/\.png$/.test(element.src)) {
                src = element.src;
                element.src = artGetStyleUrl() + 'images/spacer.gif';
            }
        }
        else {
            src = element.currentStyle.backgroundImage.match(/url\("(.+\.png)"\)/i);
            if (src) {
                src = src[1];
                element.runtimeStyle.backgroundImage = 'none';
            }
        }
        if (src) element.runtimeStyle.filter = "progid:DXImageTransform.Microsoft.AlphaImageLoader(src='" + src + "')";
    }
}

jQuery(function () {
    jQuery.each(jQuery('ul.mvc-hmenu>li:not(.mvc-hmenu-li-separator),ul.mvc-vmenu>li:not(.mvc-vmenu-separator)'), function (i, val) {
        var l = jQuery(val);
        var s = l.children('span');
        if (s.length == 0) return;
        var t = l.find('span.t').last();
        l.children('a').append(t.html(t.text()));
        s.remove();
    });
});
/* end Page */

/* begin Box, Sheet */

function artFluidSheetComputedWidth(percent, minval, maxval) {
    percent = parseInt(percent);
    var val = document.body.clientWidth / 100 * percent;
    return val < minval ? minval + 'px' : val > maxval ? maxval + 'px' : percent + '%';
}
/* end Box, Sheet */

/* begin Menu */
jQuery(function () {
    jQuery.each(jQuery('ul.mvc-hmenu>li:not(:last-child)'), function (i, val) {
        jQuery('<li class="mvc-hmenu-li-separator"><span class="mvc-hmenu-separator"> </span></li>').insertAfter(val);
    });
    if (!jQuery.browser.msie || parseInt(jQuery.browser.version) > 6) return;
    jQuery.each(jQuery('ul.mvc-hmenu li'), function (i, val) {
        val.j = jQuery(val);
        val.UL = val.j.children('ul:first');
        if (val.UL.length == 0) return;
        val.A = val.j.children('a:first');
        this.onmouseenter = function () {
            this.j.addClass('mvc-hmenuhover');
            this.UL.addClass('mvc-hmenuhoverUL');
            this.A.addClass('mvc-hmenuhoverA');
        };
        this.onmouseleave = function () {
            this.j.removeClass('mvc-hmenuhover');
            this.UL.removeClass('mvc-hmenuhoverUL');
            this.A.removeClass('mvc-hmenuhoverA');
        };

    });
});

/* end Menu */

/* begin Layout */
jQuery(function () {
    var c = jQuery('div.mvc-content');
    if (c.length !== 1) return;
    var s = c.parent().children('.mvc-layout-cell:not(.mvc-content)');

    if (jQuery.browser.msie && parseInt(jQuery.browser.version) < 8) {

        jQuery(window).bind('resize', function () {
            var w = 0;
            c.hide();
            s.each(function () {
                w += this.clientWidth;
            });
            c.w = c.parent().width();
            c.css('width', c.w - w + 'px');
            c.show();
        })

        var r = jQuery('div.mvc-content-layout-row').each(function () {
            this.c = jQuery(this).children('.mvc-layout-cell:not(.mvc-content)');
        });

        jQuery(window).bind('resize', function () {
            r.each(function () {
                if (this.h == this.clientHeight) return;
                this.c.css('height', 'auto');
                this.h = this.clientHeight;
                this.c.css('height', this.h + 'px');
            });
        });
    }

    var g = jQuery('.mvc-layout-glare-image');
    jQuery(window).bind('resize', function () {
        g.each(function () {
            var i = jQuery(this);
            i.css('height', i.parents('.mvc-layout-cell').height() + 'px');
        });
    });

    jQuery(window).trigger('resize');
});
/* end Layout */

/* begin VMenu */
jQuery(function () {
    jQuery('ul.mvc-vmenu li').not(':first').before('<li class="mvc-vsubmenu-separator"><span class="mvc-vsubmenu-separator-span"> </span></li>');
    jQuery('ul.mvc-vmenu > li.mvc-vsubmenu-separator').removeClass('mvc-vsubmenu-separator').addClass('mvc-vmenu-separator').children('span').removeClass('mvc-vsubmenu-separator-span').addClass('mvc-vmenu-separator-span');
    jQuery('ul.mvc-vmenu > li > ul > li.mvc-vsubmenu-separator:first-child').removeClass('mvc-vsubmenu-separator').addClass('mvc-vmenu-separator').addClass('mvc-vmenu-separator-first').children('span').removeClass('mvc-vsubmenu-separator-span').addClass('mvc-vmenu-separator-span');
});
/* end VMenu */

/* begin VMenuItem */
jQuery(function () {
    jQuery('ul.mvc-vmenu a').click(function () {
        var a = jQuery(this);
        a.parents('ul.mvc-vmenu').find("ul, a").removeClass('active');
        a.parent().children('ul').addClass('active');
        a.parents('ul.mvc-vmenu ul').addClass('active');
        a.parents('ul.mvc-vmenu li').children('a').addClass('active');
    });
});
/* end VMenuItem */

/* begin Button */
function artButtonSetup(className) {
    jQuery.each(jQuery("a." + className + ", button." + className + ", input." + className), function (i, val) {
        var b = jQuery(val);
        if (!b.parent().hasClass('mvc-button-wrapper')) {
            if (b.is('input')) b.val(b.val().replace(/^\s*/, '')).css('zoom', '1');
            if (!b.hasClass('mvc-button')) b.addClass('mvc-button');
            jQuery("<span class='mvc-button-wrapper'><span class='mvc-button-l'> </span><span class='mvc-button-r'> </span></span>").insertBefore(b).append(b);
            if (b.hasClass('active')) b.parent().addClass('active');
        }
        b.mouseover(function () {
            jQuery(this).parent().addClass("hover");
        });
        b.mouseout(function () {
            var b = jQuery(this);
            b.parent().removeClass("hover");
            if (!b.hasClass('active')) b.parent().removeClass('active');
        });
        b.mousedown(function () {
            var b = jQuery(this);
            b.parent().removeClass("hover");
            if (!b.hasClass('active')) b.parent().addClass('active');
        });
        b.mouseup(function () {
            var b = jQuery(this);
            if (!b.hasClass('active')) b.parent().removeClass('active');
        });
    });
}
jQuery(function () {
    artButtonSetup("mvc-button");
});

/* end Button */



