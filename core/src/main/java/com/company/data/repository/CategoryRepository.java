package com.company.data.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import com.company.data.model.items.Category;


public interface CategoryRepository extends JpaRepository<Category, Long> {
  List<Category> findByName(String name, int page, int pageSize);
}
